#include "addressbook.h"

#include <fstream>
#include <iostream>
#include <memory>

using namespace std;

// Maximum number of contacts that can be loaded into the address book.
const int MAX_CONTACTS = 100;

AddressBook::AddressBook(const string& filename)
{
    this->filename = filename;
    this->contacts = make_unique<shared_ptr<Contact>[]>(MAX_CONTACTS);
    this->contactsCount = 0;
}

// Reads the contacts from the file into memory. Creates the file if it does not exist.
// Returns true if successful, false if an I/O error occurs.
bool AddressBook::load()
{
    // If the file does not exist, create a new one based on the current state of the
    // address book (possibly empty). Return false if the file cannot be created.
    if (!fileExists() && !save()) {
        return false;
    }

    ifstream infile(filename);

    // Walk through the file, read each name and address, and add it to the book.
    string name, address;
    while (getline(infile, name) && getline(infile, address)) {
        addContact(name, address);
    }
    
    return true;
}

// Writes the contacts to the file. Returns true if successful, false if an I/O error occurs.
bool AddressBook::save() const
{
    ofstream outfile(filename);

    if (!outfile) {
        return false;
    }
    
    write(outfile);

    return true;
}

// Adds the given name and address to the book as a new contact.
bool AddressBook::addContact(const string& name, const string& address)
{
    if (contactsCount == MAX_CONTACTS) {
        return false;
    }

    contacts[contactsCount++] = make_shared<Contact>(name, address);
    return true;
}

// Prints all contacts to the console.
void AddressBook::print() const
{
    write(cout);
}

// Prints all contacts to the given output stream.
void AddressBook::write(ostream &out) const
{
    for (int i = 0; i < contactsCount; i++) {
        contacts[i]->write(out);
    }    
}

// Returns true if the file exists; false otherwise.
bool AddressBook::fileExists() const
{
    ifstream file(filename);
    return file.good();
}
