#include "addressbook.h"

#include <iostream>

using namespace std;

void printUsage()
{
    cout << "Usage:" << endl;
    cout << "   addressbook" << endl;
    cout << "   addressbook NAME ADDRESS" << endl;
}

int main(int argc, char **argv)
{
    AddressBook addressBook("addresses.txt");
    
    if (!addressBook.load()) {
        cerr << "Could not load address book" << endl;
        return 1;
    }

    switch (argc) {
        case 1:
            addressBook.print();
            break;
        
        case 3:
            if (!addressBook.addContact(argv[1], argv[2])) {
                cerr << "Could not add contact" << endl;
                return 1;
            }
            if (!addressBook.save()) {
                cerr << "Could not save address book" << endl;
                return 1;
            }
            break;
        
        default:
            printUsage();
            return 1;
    }

    return 0;
}
