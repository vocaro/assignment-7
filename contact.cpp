#include "contact.h"

using namespace std;

Contact::Contact(const string &name, const string &address)
{
    this->name = name;
    this->address = address;
}

// Writes the name and address to the given output stream, one per line.
void Contact::write(ostream &out) const
{
    out << name << endl;
    out << address << endl;
}
