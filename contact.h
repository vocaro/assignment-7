#pragma once

#include <ostream>
#include <string>

class Contact
{
private:
    std::string name;
    std::string address;
public:
    Contact(const std::string &name, const std::string &address);
    void write(std::ostream &out) const;
};
