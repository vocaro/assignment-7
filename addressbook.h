#pragma once

#include "contact.h"

#include <memory>
#include <ostream>
#include <string>

class AddressBook {
private:
    std::string filename;
    std::unique_ptr<std::shared_ptr<Contact>[]> contacts;
    int contactsCount;
    void write(std::ostream &out) const;
    bool fileExists() const;
public:
    AddressBook(const std::string& filename);
    bool load();
    bool save() const;
    bool addContact(const std::string& name, const std::string& address);  
    void print() const;
};
